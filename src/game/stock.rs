use termion::{
    cursor,
    input::MouseTerminal,
};
use std::io::{Write, Error};
use crate::game::{
    card::Card,
    coordinates::Coordinate
};

/// stock window
pub struct Stock {
    /// stock pile
    pub pile: Vec<Card>,
    /// turned over cards
    pub waste: Vec<Card>
}
impl Stock {
    /// create new stock instance
    pub fn new() -> Stock {
        Stock {
            pile: Vec::new(),
            waste: Vec::new()
        }
    }
    /// draws the stock pile window and the piles
    pub fn draw<W: Write>(&self, stdout: &mut MouseTerminal<W>, coords: Coordinate) -> Result<(), Error> {
        let (x, y) = coords;

        // render base
        write!(
            stdout,
            "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
            cursor::Goto(x, y),
            "┌─stock╶───────────────────┐",
            cursor::Goto(x, y+1),
            "│  ╭────────╮  ╭────────╮  │",
            cursor::Goto(x, y+2),
            "│  │        │  │        │  │",
            cursor::Goto(x, y+3),
            "│  │        │  │        │  │",
            cursor::Goto(x, y+4),
            "│  │        │  │        │  │",
            cursor::Goto(x, y+5),
            "│  │        │  │        │  │",
            cursor::Goto(x, y+6),
            "│  │        │  │        │  │",
            cursor::Goto(x, y+7),
            "│  ╰────────╯  ╰────────╯  │",
            cursor::Goto(x, y+8),
            "└──────────────────────────┘",
        )?;

        // render card stacks
        if !self.pile.is_empty() {
            self.pile.last().unwrap().draw(stdout, (x+3, y+1))?;
        }
        if !self.waste.is_empty() {
            self.waste.last().unwrap().draw(stdout, (x+15, y+1))?;
        }

        Ok(())
    }

    /// flips next card from stock to waste
    /// return true if there was no need to reset
    pub fn next(&mut self) -> bool {
        if self.pile.is_empty() && !self.waste.is_empty() {
            // flip waste
            for _ in 0..self.waste.len() {
                if let Some(mut card) = self.waste.pop() {
                    card.visible=false;
                    self.pile.push(card);
                }
            }
            return false;
        }

        if !self.pile.is_empty() {
            // flip card
            if let Some(mut card) = self.pile.pop() {
                card.visible=true;
                self.waste.push(card);
            }
        }

        true
    }
}
