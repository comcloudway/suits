use termion::{
    cursor,
    input::MouseTerminal,
};
use std::io::{Write, Error};
use crate::game::{
    coordinates::Coordinate,
    card::{Card, CardValue}
};

/// represents the foundation window
pub struct Foundation {
    pub slot0: Vec<Card>,
    pub slot1: Vec<Card>,
    pub slot2: Vec<Card>,
    pub slot3: Vec<Card>,
}
impl Foundation {
    /// initialised foundation object
    pub fn new() -> Foundation {
        Foundation {
            slot0: Vec::new(),
            slot1: Vec::new(),
            slot2: Vec::new(),
            slot3: Vec::new(),
        }
    }

    /// draws foundation and slot items
    pub fn draw<W: Write>(&self, stdout: &mut MouseTerminal<W>, coords: Coordinate) -> Result<(), Error> {
        let (x, y) = coords;
        write!(
            stdout,
            "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
            cursor::Goto(x,y),
            "┌─foundation╶────────────────────────────────────┐",
            cursor::Goto(x,y+1),
            "│ ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮ │",
            cursor::Goto(x,y+2),
            "│ │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+3),
            "│ │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+4),
            "│ │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+5),
            "│ │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+6),
            "│ │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+7),
            "│ ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯ │",
            cursor::Goto(x,y+8),
            "└────────────────────────────────────────────────┘",
        )?;

        if !self.slot0.is_empty() {
            self.slot0.last().unwrap().draw(stdout, (x+2, y+1))?;
        }
        if !self.slot1.is_empty() {
            self.slot1.last().unwrap().draw(stdout, (x+2+12, y+1))?;
        }
        if !self.slot2.is_empty() {
            self.slot2.last().unwrap().draw(stdout, (x+2+12*2, y+1))?;
        }
        if !self.slot3.is_empty() {
            self.slot3.last().unwrap().draw(stdout, (x+2+12*3, y+1))?;
        }

        Ok(())
    }

    /// checks if the card can be added to the stack
    /// returns true if move is possible
    pub fn is_valid(&self, card: &Card, slot: u8) -> bool {
        if slot>3 {
            return false;
        } else {
            let v = match slot {
                0=>&self.slot0,
                1=>&self.slot1,
                2=>&self.slot2,
                _=>&self.slot3,
            };

            if v.is_empty() {
                // card has be an ace
                if let CardValue::Ace = card.value {
                    return true;
                } else {
                    return false;
                }
            } else {
                // card as to be of same color as previous card
                // and one number higher
                let last:&Card = v.last().unwrap();
                if card.value.to_num() != last.value.to_num()+1 {
                    return false;
                }

                if last.suit != card.suit {
                    return false;
                }

                return true;
            }
        }
    }

    // get a non mutable reference to the given slot
    pub fn get(&self, slot: u8) -> &Vec<Card> {
        match slot {
            0=>&self.slot0,
            1=>&self.slot1,
            2=>&self.slot2,
            _=>&self.slot3,
        }
    }

    /// gets a mutable reference to the given slot
    pub fn get_mut(&mut self, slot: u8) -> &mut Vec<Card> {
        match slot {
            0=>&mut self.slot0,
            1=>&mut self.slot1,
            2=>&mut self.slot2,
            _=>&mut self.slot3,
        }
    }
}
