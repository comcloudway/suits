use termion::{
    color,
    cursor,
    input::MouseTerminal,
};
use std::io::{Write, Error};
use crate::game::coordinates::Coordinate;

/// stores player score
pub struct Score {
    /// points gained
    pub gained: usize,
    /// points lost
    pub lost: usize
}
impl Score {
    /// initialize a new score struct
    pub fn new() -> Score {
        Score {
            gained: 0,
            lost: 0
        }
    }
    /// calculate score
    pub fn calc(&self) -> isize {
        return self.gained as isize - self.lost as isize;
    }
    /// renders the score:
    /// calculated-score (gained/lost)
    pub fn draw<W: Write>(&self, stdout: &mut MouseTerminal<W>, coords: Coordinate) -> Result<(), Error> {
        write!(
            stdout,
            "{}{} ({}+{}{}/{}-{}{})",
            cursor::Goto(coords.0, coords.1),
            self.calc(),
            color::Fg(color::Green),
            self.gained,
            color::Fg(color::Reset),
            color::Fg(color::Red),
            self.lost,
            color::Fg(color::Reset),
        )
    }
}
