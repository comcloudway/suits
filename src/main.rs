extern crate termion;
mod game;

use termion::{
    event::*,
    cursor,
    color,
    input::{TermRead, MouseTerminal},
    raw::IntoRawMode};
use std::io::{self, Write};
use crate::game::{
    Game,
    tableau::Tableau,
    card::Card,
    coordinates::{
        Coordinate,
        STOCK_COORDS,
        FOUNDATION_COORDS,
        TABLEAU_COORDS,
    }
};


/// more information about the move to be executed
pub enum MoveOperation {
    /// waste item to move to which foundation
    WasteToFoundation(u8),
    /// waste item to move to which tableau slot
    WasteToTableau(u8),
    /// which stack to move to which foundation pile
    TableauToFoundation(u8, u8),
    /// which foundation card to move to which pile
    FoundationToTableau(u8, u8),
    /// how many cards to move from tableau 1 to tableau 2
    TableauToTableau(u8, u8, u8)
}

/// Operation to run, e.g. after resolving Selection
pub enum Operation {
    None,

    Move(MoveOperation),
    TurnTableauCard,

    TurnStock,
}


/*
+   +    +    +    +    +    +    +    +    +    +    +    +    +    +    +    +    +
┌─stock────────────────────┐        ┌─foundation─────────────────────────────────────┐
│  ╭────────╮  ╭────────╮  │        │ ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮ │
│  │        │  │        │  │        │ │        │  │        │  │        │  │        │ │
│  │        │  │        │  │        │ │        │  │        │  │        │  │        │ │
│  │        │  │        │  │        │ │        │  │        │  │        │  │        │ │
│  │        │  │        │  │        │ │        │  │        │  │        │  │        │ │
│  │        │  │        │  │        │ │        │  │        │  │        │  │        │ │
│  ╰────────╯  ╰────────╯  │        │ ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯ │
└──────────────────────────┘        └────────────────────────────────────────────────┘

┌─tableau────────────────────────────────────────────────────────────────────────────┐
│ ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮ │
│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │
│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │
│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │
│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │
│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │
│ ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯ │
└────────────────────────────────────────────────────────────────────────────────────┘

calc (gained/lost)
 */

/// an enum containing all UI Elements that can be selected to run a specified action
#[derive(PartialEq)]
enum UiClickableElement {
    /// Stock Window > Pile
    StockPile,
    /// Stock Window > Waste
    StockWaste,

    /// Foundation Window > Slot 1
    Foundation1,
    /// Foundation Window > Slot 2
    Foundation2,
    /// Foundation Window > Slot 3
    Foundation3,
    /// Foundation Window > Slot 4
    Foundation4,

    /// card in tableau slot at index
    TableauSlot(u8, u8)
}
impl UiClickableElement {
    pub fn from_coords(coords: Coordinate, tableau: &Tableau) -> Option<UiClickableElement> {
        let (x,y) = coords;

        if x>STOCK_COORDS.0 && y>STOCK_COORDS.1 && x<STOCK_COORDS.0+27 && y<STOCK_COORDS.1+8 {
            // stock window
            if x>STOCK_COORDS.0+1 && y>STOCK_COORDS.1+1 && x<=STOCK_COORDS.0+11 && y<STOCK_COORDS.1+7 {
                return Some(UiClickableElement::StockPile);
            } else if x>STOCK_COORDS.0+1+10 && y>STOCK_COORDS.1+1 && x<=STOCK_COORDS.0+11+11 && y<STOCK_COORDS.1+7 {
                return Some(UiClickableElement::StockWaste);
            }
        } else if x>FOUNDATION_COORDS.0 && y>FOUNDATION_COORDS.1 && x<FOUNDATION_COORDS.0+49 && y<FOUNDATION_COORDS.1+8 {
            // foundation window
            if x>FOUNDATION_COORDS.0+1 && y>FOUNDATION_COORDS.1+1 && y<FOUNDATION_COORDS.1+8 {
                if x<FOUNDATION_COORDS.0+11+2 {
                    // slot 1
                    return Some(UiClickableElement::Foundation1);
                } else if x<FOUNDATION_COORDS.0+11*2+2*2 {
                    // slot 2
                    return Some(UiClickableElement::Foundation2);
                } else if x<FOUNDATION_COORDS.0+11*3+2*3 {
                    // slot 3
                    return Some(UiClickableElement::Foundation3);
                } else if x<FOUNDATION_COORDS.0+11*4+2*3 {
                    // slot 4
                    return Some(UiClickableElement::Foundation4);
                }
            }
        } else if x>TABLEAU_COORDS.0 && y>TABLEAU_COORDS.1 && x<TABLEAU_COORDS.0+85 && y<TABLEAU_COORDS.1+8+tableau.max() as u16*2 {
            // tableau window
            for slot in 0..7 {
                if x>TABLEAU_COORDS.0+1+10*slot+2*slot && y>TABLEAU_COORDS.1+1 && x<TABLEAU_COORDS.0+1+10*(slot+1)+2*slot {
                    // column detected
                    // TODO find card
                    let mut offset = 0;
                    let l:u16 = tableau.get(slot as u8).len() as u16;
                    if l > 1 {
                        offset=l-1;
                        // more than one card
                        for c in 0..l {
                            if y>TABLEAU_COORDS.1+1+c*2 && y<TABLEAU_COORDS.1+1+2+c*2 {
                                return Some(UiClickableElement::TableauSlot(slot as u8, c as u8));
                            }
                        }
                    }

                    if y>TABLEAU_COORDS.1+1+offset*2 && y < TABLEAU_COORDS.1+1+8+offset*2 {
                        // first card
                        return Some(UiClickableElement::TableauSlot(slot as u8, offset as u8));
                    }
                }
            }
        }

        None
    }
}

fn main() {
    let stdin = io::stdin();
    let mut stdout = MouseTerminal::from(io::stdout().into_raw_mode().unwrap());

    writeln!(stdout,
             "{}{}{}",
             termion::clear::All,
             cursor::Hide,
             cursor::Goto(1, 1))
        .unwrap();

    let mut game = Game::new();

    game.draw(&mut stdout).unwrap();
    stdout.flush().unwrap();

    let mut old_coords:Coordinate = (0,0);
    let mut selection: Option<UiClickableElement> = None;

    for c in stdin.events() {
        let mut update = false;
        let mut operation: Operation = Operation::None;

        // TODO: handle event
        let evt = c.unwrap();
        match evt {
            Event::Key(k) => match k {
                Key::Char(c) => match c {
                    'q'=>break,
                    'r'=>{
                        operation=Operation::None;

                        update=true;
                        game=Game::new();
                    }
                    _=>()
                }
                _ => (),
            },
            Event::Mouse(me) => {
                match me {
                    MouseEvent::Press(_, x, y) => {
                        old_coords=(x,y);
                    }
                    MouseEvent::Release(x, y) => {
                        if old_coords.0 != x || old_coords.1 != y {
                            // check if moved to different card
                            // set different
                            if let Some(o) = UiClickableElement::from_coords(old_coords, &game.tableau) {
                                if let Some(c) = UiClickableElement::from_coords((x,y), &game.tableau) {
                                    if o!=c {
                                        // TODO move operation
                                        selection=Some(o);
                                    }
                                }
                            }
                        }

                        // click event
                        if let Some(item) = UiClickableElement::from_coords((x,y), &game.tableau) {
                            match item {
                                UiClickableElement::StockPile => {
                                    selection=None;
                                    operation=Operation::TurnStock;
                                },
                                UiClickableElement::StockWaste => {
                                    if let Some(_) = selection {
                                        // dismiss
                                    } else {
                                        // select waste
                                        selection=Some(item);
                                    }
                                },
                                UiClickableElement::TableauSlot(slot, card) => {
                                    if let Some(source) = &selection {
                                        // move item to tableau
                                        match source {
                                            UiClickableElement::StockWaste => {
                                                operation=Operation::Move(MoveOperation::WasteToTableau(slot));
                                                selection=None;
                                            },
                                            UiClickableElement::TableauSlot(os, oc) => {
                                                operation=Operation::Move(MoveOperation::TableauToTableau(*oc, *os, slot));
                                                selection=None;
                                            }
                                            UiClickableElement::Foundation1 |
                                            UiClickableElement::Foundation2 |
                                            UiClickableElement::Foundation3 |
                                            UiClickableElement::Foundation4 => {
                                                operation=Operation::Move(MoveOperation::FoundationToTableau(match item {
                                                    UiClickableElement::Foundation1 => 0,
                                                    UiClickableElement::Foundation2 => 1,
                                                    UiClickableElement::Foundation3 => 2,
                                                    UiClickableElement::Foundation4 => 3,
                                                    // fake item
                                                    _ => 5
                                                }, slot));
                                                selection=None;
                                            },
                                            _ => ()
                                        }
                                    } else {
                                        match game.tableau.get(slot).get(card as usize) {
                                            Some(c) => {
                                                selection=Some(item);
                                                if !c.visible {
                                                    // flip card
                                                    operation=Operation::TurnTableauCard
                                                }
                                            },
                                            None=>()
                                        }
                                    }
                                },
                                UiClickableElement::Foundation1 |
                                UiClickableElement::Foundation2 |
                                UiClickableElement::Foundation3 |
                                UiClickableElement::Foundation4 => {
                                    if let Some(source) = &selection {
                                        // move item to foundation
                                        match source {
                                            UiClickableElement::StockWaste => {
                                                operation=Operation::Move(MoveOperation::WasteToFoundation(match item {
                                                    UiClickableElement::Foundation1 => 0,
                                                    UiClickableElement::Foundation2 => 1,
                                                    UiClickableElement::Foundation3 => 2,
                                                    UiClickableElement::Foundation4 => 3,
                                                    // fake item
                                                    _ => 5
                                                }));
                                                selection=None;
                                            },
                                            UiClickableElement::TableauSlot(slot, _) => {
                                                operation=Operation::Move(MoveOperation::TableauToFoundation(*slot, match item {
                                                    UiClickableElement::Foundation1 => 0,
                                                    UiClickableElement::Foundation2 => 1,
                                                    UiClickableElement::Foundation3 => 2,
                                                    UiClickableElement::Foundation4 => 3,
                                                    // fake item
                                                    _ => 5
                                                }));
                                                selection=None;
                                            },
                                            _ => ()
                                        }
                                    } else {
                                        // select foundation as source
                                        selection=Some(item);
                                    }
                                }
                            }
                        }
                    }
                    _ => ()
                }
            }
            _ => {}
        }

        // TODO: handle operation
        match operation {
            Operation::TurnStock => {
                let had_to_reset = !game.stock.next();
                if had_to_reset {
                    game.score.lost+=100;
                }
                update=true;
            },
            Operation::Move(m) => match m {
                MoveOperation::TableauToTableau(count, so, st) => {
                    let mut m:Vec<Card> = game.tableau.get_mut(so).drain(count as usize..).collect();
                    if game.tableau.is_valid(m.first().unwrap(), st) {
                        update = true;
                        game.tableau.get_mut(st).append(&mut m);
                    } else {
                        game.tableau.get_mut(so).append(&mut m);
                    }
                },
                MoveOperation::WasteToTableau(s) => {
                    if let Some(card) = game.stock.waste.pop() {
                        if game.tableau.is_valid(&card, s) {
                            update=true;
                            game.score.gained+=5;
                            game.tableau.get_mut(s).push(card);
                        } else {
                            game.stock.waste.push(card);
                        }
                    }
                },
                MoveOperation::WasteToFoundation(s) => {
                    if s < 4 {
                        if let Some(card) = game.stock.waste.pop() {
                            if game.foundation.is_valid(&card, s) {
                                update=true;
                                game.score.gained+=10;
                                game.foundation.get_mut(s).push(card);
                            } else {
                                game.stock.waste.push(card);
                            }
                        }
                    }
                },
                MoveOperation::TableauToFoundation(t,f) => {
                    if let Some(card) = game.tableau.get_mut(t).pop() {
                        if game.foundation.is_valid(&card, f) {
                            update=true;
                            game.score.gained+=10;
                            game.foundation.get_mut(f).push(card);
                        } else {
                            game.tableau.get_mut(t).push(card);
                        }
                    }
                },
                MoveOperation::FoundationToTableau(f,t) => {
                    if let Some(card) = game.foundation.get_mut(f).pop() {
                        if game.tableau.is_valid(&card, t) {
                            update=true;
                            game.score.lost+=15;
                            game.tableau.get_mut(t).push(card);
                        } else {
                            game.foundation.get_mut(f).push(card);
                        }
                    }
                },
            },
            Operation::TurnTableauCard => {
                if let Some(s) = &selection {
                    match s {
                        UiClickableElement::TableauSlot(s,c) => {
                            let slot = game.tableau.get_mut(*s);
                            if slot.len()==*c as usize+1 {
                                if let Some(card) = slot.get_mut(*c as usize) {
                                    update=true;
                                    game.score.gained+=5;
                                    card.visible=true;
                                }
                            }
                            selection=None;
                        },
                        _ => ()
                    }}
            },
            _ => ()
        };

        // TODO: render
        if update {
            writeln!(stdout,
                     "{}{}",
                     termion::clear::All,
                     cursor::Goto(1, 1))
                .unwrap();
            game.draw(&mut stdout).unwrap();
            stdout.flush().unwrap();
        }

        // TODO check if game is finished
        if game.is_won() {
            break;
        }
    }

    if game.is_won() {
        write!(
            stdout,
            "{}{}{}Well done{}{}Your final score is: {}{}During the game you earned {}{}{} points,{}and lost {}{}{}{}.",
            termion::clear::All,
            cursor::Goto(1, 1),
            color::Fg(color::Blue),
            color::Fg(color::Reset),
            cursor::Goto(1, 3),
            game.score.calc(),
            cursor::Goto(1, 4),
            color::Fg(color::Green),
            game.score.gained,
            color::Fg(color::Reset),
            cursor::Goto(1, 5),
            color::Fg(color::Red),
            game.score.lost,
            color::Fg(color::Reset),
            cursor::Show
        ).unwrap();
    } else {
        write!(
            stdout,
            "{}{}Try again next time{}",
            termion::clear::All,
            cursor::Goto(1, 1),
            cursor::Show
        ).unwrap();
    }
    stdout.flush().unwrap();
}
