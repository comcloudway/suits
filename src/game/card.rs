use termion::{
    color,
    cursor,
    input::MouseTerminal,
};
use std::io::{Write, Error};
use crate::game::coordinates::Coordinate;

/// The type/suit of playing card
#[derive(Copy, Clone, PartialEq)]
pub enum CardSuit {
    Clubs,
    Diamonds,
    Hearts,
    Spades
}
impl CardSuit {
    /// converts the cardtype to a color value
    /// used to render card
    pub fn to_color(&self) -> &dyn color::Color {
        match self {
            CardSuit::Clubs | CardSuit::Spades => &color::White,
            CardSuit::Diamonds | CardSuit::Hearts => &color::Red,
        }
    }
    /// converts the cardtype to a char
    /// used to render card
    pub fn to_char(&self) -> char {
        match self {
            CardSuit::Clubs => '♣',
            CardSuit::Spades => '♠',
            CardSuit::Diamonds => '◆',
            CardSuit::Hearts => '♥'
        }
    }
}
/// the card value
#[derive(Copy, Clone)]
pub enum CardValue {
    Ace,
    /// a numeric card value - from 2 to 10
    Number(u8),
    Jack,
    Queen,
    King
}
impl CardValue {
    /// try to create a CardValue from a number
    pub fn from_u8(num: u8) -> Option<CardValue> {
        match num {
            1 => Some(CardValue::Ace),
            11 => Some(CardValue::Jack),
            12 => Some(CardValue::Queen),
            13 => Some(CardValue::King),
            n => if n<11 {Some(CardValue::Number(n))} else {None}
        }
    }
    /// converts the card value to a char
    /// used to render card
    /// if number>=10 returns a ?, because number wouldn't be of type char
    pub fn to_char(&self) -> char {
        match self {
            CardValue::Ace => 'A',
            CardValue::Number(num)=>match char::from_digit(*num as u32, 10) {
                Some(c) => c,
                None => '⒑'
            },
            CardValue::Jack => 'J',
            CardValue::King => 'K',
            CardValue::Queen => 'Q',
        }
    }
    /// returns the numeric card value
    pub fn to_num(&self) -> u8 {
        match self {
            CardValue::Ace => 1,
            CardValue::Number(n) => *n,
            CardValue::Jack => 11,
            CardValue::Queen => 12,
            CardValue::King => 13,
        }
    }
}

#[derive(Copy, Clone)]
pub struct Card {
    pub suit: CardSuit,
    pub value: CardValue,
    pub visible: bool
}
impl Card {
    pub fn new(suit: CardSuit, value: CardValue, visible: bool) -> Card {
        Card {
            suit,
            value,
            visible
        }
    }
    /// draws the cards at the given position
    pub fn draw<W: Write>(&self, stdout: &mut MouseTerminal<W>, coords: Coordinate) -> Result<(), Error>{
        let (x, y) = coords;
        if self.visible {
            let c = self.suit.to_color();
            let s = self.suit.to_char();
            let v = self.value.to_char();
            write!(
                stdout,
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                cursor::Goto(x, y),
                "╭────────╮",
                cursor::Goto(x, y+1),
                "│ ",
                color::Fg(c),
                v,
                "    ",
                s,
                color::Fg(color::Reset),
                " │",
                cursor::Goto(x, y+2),
                "│        │",
                cursor::Goto(x, y+3),
                "│        │",
                cursor::Goto(x, y+4),
                "│        │",
                cursor::Goto(x, y+5),
                "│ ",
                color::Fg(c),
                s,
                "    ",
                v,
                color::Fg(color::Reset),
                " │",
                cursor::Goto(x, y+6),
                "╰────────╯",
            )
        } else {
            write!(
                stdout,
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                cursor::Goto(x, y),
                "╭────────╮",
                cursor::Goto(x, y+1),
                "│╬╬╬╬╬╬╬╬│",
                cursor::Goto(x, y+2),
                "│╬╬╬╬╬╬╬╬│",
                cursor::Goto(x, y+3),
                "│╬╬╬╬╬╬╬╬│",
                cursor::Goto(x, y+4),
                "│╬╬╬╬╬╬╬╬│",
                cursor::Goto(x, y+5),
                "│╬╬╬╬╬╬╬╬│",
                cursor::Goto(x, y+6),
                "╰────────╯",
            )
        }
    }
}
