extern crate termion;
pub mod tableau;
pub mod coordinates;
pub mod card;
pub mod foundation;
pub mod stock;
pub mod deck;
pub mod score;

use termion::{
    input::MouseTerminal,
};
use std::io::{Write, Error};
use self::{
    tableau::Tableau,
    foundation::Foundation,
    stock::Stock,
    score::Score,
    deck::Deck,
    coordinates::{
        STOCK_COORDS,
        FOUNDATION_COORDS,
        TABLEAU_COORDS,
        SCORE_COORDS,
    }
};

/// struct to store the game data
pub struct Game {
    /// stock piles
    pub stock: Stock,
    /// store score
    pub score: Score,
    /// store foundation
    pub foundation: Foundation,
    /// store tableau
    pub tableau: Tableau
}
impl Game {
    /// start new game
    pub fn new() -> Game {
        let mut g = Game {
            stock: Stock::new(),
            score: Score::new(),
            foundation: Foundation::new(),
            tableau: Tableau::new()
        };

        // generate cards
        let mut deck = Deck::new();
        deck.shuffle();

        // assign cards
        for s in 1..8 {
            for c in 0..s {
                if let Some(mut card) = deck.0.pop() {
                    if c==s-1 {
                        card.visible=true;
                    }
                    (match s-1 {
                        0 => &mut g.tableau.slot0,
                        1 => &mut g.tableau.slot1,
                        2 => &mut g.tableau.slot2,
                        3 => &mut g.tableau.slot3,
                        4 => &mut g.tableau.slot4,
                        5 => &mut g.tableau.slot5,
                        _ => &mut g.tableau.slot6
                    }).push(card);
                }
            }
        }

        g.stock.pile=deck.0;

        return g;
    }

    /// draw all components
    pub fn draw<W: Write>(&self, stdout: &mut MouseTerminal<W>) -> Result<(), Error> {
        self.stock.draw(stdout, STOCK_COORDS)?;
        self.foundation.draw(stdout, FOUNDATION_COORDS)?;
        self.tableau.draw(stdout, TABLEAU_COORDS)?;
        self.score.draw(stdout, (SCORE_COORDS.0, 2*self.tableau.max() as u16+SCORE_COORDS.1))?;

        Ok(())
    }

    /// check if game is won
    pub fn is_won(&self) -> bool {
        for f in 0..4 {
            if self.foundation.get(f).len() != 13 {
                return false;
            }
        }

        true
    }
}
