use crate::game::card::{Card, CardSuit, CardValue};
use rand::seq::SliceRandom;

pub struct Deck(pub Vec<Card>);
impl Deck {
    /// create a new deck including all suits and cards
    pub fn new() -> Deck {
        let mut deck = Vec::new();
        for suit in [CardSuit::Clubs, CardSuit::Diamonds, CardSuit::Hearts, CardSuit::Spades] {
            for value in 1..13+1 {
                deck.push(Card::new(
                    suit,
                    if let Some(n) = CardValue::from_u8(value) {n} else {CardValue::Number(1)},
                    false));
            }
        }
        return Deck(deck);
    }
    /// mix the given deck
    pub fn shuffle(&mut self) {
        self.0.shuffle(&mut rand::thread_rng());
    }
}
