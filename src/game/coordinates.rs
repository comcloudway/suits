/// a coordinate in row, column
pub type Coordinate = (u16,u16);

/// top left corner of stock window
pub const STOCK_COORDS: Coordinate = (1,1);
/// top left corner of foundation window
pub const FOUNDATION_COORDS: Coordinate = (37,1);
/// top left corner of tableau window
pub const TABLEAU_COORDS: Coordinate = (1,12);
/// top left corner of score line
pub const SCORE_COORDS: Coordinate = (1,22);
