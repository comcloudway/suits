extern crate termion;

use termion::{
    cursor,
    input::MouseTerminal,
};
use std::io::{Write, Error};
use crate::game::{
    card::{Card, CardSuit, CardValue},
    coordinates::Coordinate
};

/// represents table
pub struct Tableau {
    pub slot0: Vec<Card>,
    pub slot1: Vec<Card>,
    pub slot2: Vec<Card>,
    pub slot3: Vec<Card>,
    pub slot4: Vec<Card>,
    pub slot5: Vec<Card>,
    pub slot6: Vec<Card>,
}
impl Tableau {
    /// create empty tableau
    pub fn new() -> Tableau {
        Tableau {
            slot0: Vec::new(),
            slot1: Vec::new(),
            slot2: Vec::new(),
            slot3: Vec::new(),
            slot4: Vec::new(),
            slot5: Vec::new(),
            slot6: Vec::new(),
        }
    }
    /// draws tableau at given position
    pub fn draw<W:Write>(&self, stdout: &mut MouseTerminal<W>, coords:Coordinate) -> Result<(), Error> {
        let (x, y) = coords;
        // draw base layout
        write!(
            stdout,
            "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
            cursor::Goto(x,y),
            "┌─tableau╶───────────────────────────────────────────────────────────────────────────┐",
            cursor::Goto(x,y+1),
            "│ ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮  ╭────────╮ │",
            cursor::Goto(x,y+2),
            "│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+3),
            "│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+4),
            "│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+5),
            "│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+6),
            "│ │        │  │        │  │        │  │        │  │        │  │        │  │        │ │",
            cursor::Goto(x,y+7),
            "│ ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯  ╰────────╯ │",
        )?;

        // auto adjust height
        let m = self.max();

        write!(
            stdout,
            "{}└────────────────────────────────────────────────────────────────────────────────────┘",
            cursor::Goto(x, y + 8 + m as u16 * 2)
        )?;

        for i in 0..m*2 {
            write!(
                stdout,
                "{}│                                                                                    │",
                cursor::Goto(x, y+8+i as u16),
            )?;
        }

        // draw cards in slots
        for slot in 0..7 {
            let s = self.get(slot as u8);

            let xx = x + slot*12 + 2;
            for (c, card) in s.iter().enumerate() {
                card.draw(stdout, (xx, 1 + y + c as u16*2))?;
            }
        }

        Ok(())
    }

    /// returns length of longest slot
    pub fn max(&self) -> usize {
        if let Some(len) = [self.slot0.len(),
                            self.slot1.len(),
                            self.slot2.len(),
                            self.slot3.len(),
                            self.slot4.len(),
                            self.slot5.len(),
                            self.slot6.len()].iter().max() {
            return *len;
        } else {
            return 0;
        }
    }

    /// returns a non-mutable reference to a numeric slot
    pub fn get(&self, slot:u8) -> &Vec<Card> {
        match slot {
            0 => &self.slot0,
            1 => &self.slot1,
            2 => &self.slot2,
            3 => &self.slot3,
            4 => &self.slot4,
            5 => &self.slot5,
            _ => &self.slot6
        }
    }

    /// returns a mutable reference to a numeric slot
    pub fn get_mut(&mut self, slot:u8) -> &mut Vec<Card> {
        match slot {
            0 => &mut self.slot0,
            1 => &mut self.slot1,
            2 => &mut self.slot2,
            3 => &mut self.slot3,
            4 => &mut self.slot4,
            5 => &mut self.slot5,
            _ => &mut self.slot6
        }
    }

    /// validate a move
    /// return true if allows
    pub fn is_valid(&self, card: &Card, slot: u8) -> bool {
        // invalid slot
        if slot > 6 {
            return false;
        }

        let s = self.get(slot);
        if s.is_empty() {
            // card to be king
            if let CardValue::King = card.value {
                return true;
            } else {
                return false;
            }
        } else {
            // card has to be of opposing color and a higher numeric value
            let v = card.value.to_num();
            let last: &Card = s.last().unwrap();

            if last.value.to_num() == v+1 {
                return match card.suit {
                    CardSuit::Clubs | CardSuit::Spades => match last.suit {
                        CardSuit::Clubs | CardSuit::Spades => false,
                        _ => true
                    },
                    CardSuit::Diamonds | CardSuit::Hearts => match last.suit {
                        CardSuit::Diamonds | CardSuit::Hearts => false,
                        _ => true
                    },
                }

            }
            return false
        }
    }
}
